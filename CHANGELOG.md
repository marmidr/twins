# Changelog

See the [keepachangelog.com description](https://keepachangelog.com/en/1.0.0/).

## 0.19.3 - 2025-02-11

* Added
  * CHANGELOG
  * Demo: tracking the widgets drawer
* Changed
  * tests moved outside the `lib/` folder
  * BitBucket CI updated
* Deprecated
* Removed
* Fixed
